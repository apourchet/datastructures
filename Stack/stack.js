var Stack = function() {
    this.count = 0;
    this.head = null;
};

var StackItem = function(value, next) {
    this.value = value;
    this.next = next;
};

Stack.prototype.push = function(value) {
    this.head = new StackItem(value, this.head);
    this.count++;
    return this.head;
};

Stack.prototype.pop = function() {
    if (!this.head) return null;
    var prevVal = this.head.value;
    this.head = this.head.next;
    return prevVal;
};

Stack.prototype.peek = function() {
    if (!this.head) return null;
    return this.head.value;
};

Stack.prototype.toString = function(callback) {
    var item = this.head;
    var str = "Stack: ";
    if (callback) {
        while (item != null) {
            str += "\n" + callback(item.value);
            item = item.next;
        }
	} else {
        while (item != null) {
            str += "\n" + (item.value);
            item = item.next;
        }
    }
    return str;
};

Stack.prototype.popAll = function(callback) {
    var i;
    while (i = this.pop()) {
            callback(i);
    }
};